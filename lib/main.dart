import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
  /* Ejecicio 1 */
  ///////////////////////////////////////////////////////////////////////////////
   bool sameNumberReversed(int i) {
     /* 
        Convertimos el entero que recibimos a una cadena de texto
        para poder acceder a cada uno de sus caracteres 
     */ 
    String number = '$i';

    /* 
      Los Ceros iniciales y finales son omitidos 
      ya que los ceros a la izquierda no son considerados
    */
    number = number.replaceAll(RegExp('0+\$'), '');

     /* 
       Creamos una variable donde su valor es el numero que recibimos
       y lo leemos de derecha a izq con el iterable reversed.join
     */
    String reversedNumber = number.split('').reversed.join('').toString();

     /* 
       Validamos con la condicion == que el numero que recibimos sea igual al numero que leemos
       de derecha a izq. si esto se cumple regresa un true caso contrario false;
     */
   
    return number == reversedNumber;
  }


  int nextPrimeAndReversed(int n) {
    /* 
      Se declara la variable, se le asigna el numero recibido y se le suma uno
      para buscar apartir del siguiente numero
    */
    int currentNumber = n + 1;
    /*
      Se implementa un bucle while en true para que se ejecute hasta 
      encontrar un numero primo que pueda ser leido de izq a derecha y viceversa
    */
    while (true) {
      bool isPrime = true;  
      
      /* 
        Ciclo for para determinar si es un numero primo, si no lo es 
        isPrime su valor pasa a false.  Una vez que sea un numero primo 
        se mantiene en true; 
      */
      for (int i = 2; i <= currentNumber / 2; i++) {
        if (currentNumber % i == 0) {
          isPrime = false;
        }
      }
      
      /* 
        Implementamos una condición para cuando un numero primo sea verdadero y
        sea el mismo de izquierda a derecha y vicerversa, 
        me retorne el numero primo siguiente y se detiene el bucle While;
        utilizamos la funcion anteriormente creada sameNumberReversed();
      */
      if (isPrime && sameNumberReversed(currentNumber)) {
        return currentNumber;
      }

      /* 
        Si la condicion anterior no se cumple se incrementa el numero
        recibido y vuelve a iniciar el bucle while;
      */
      currentNumber++;
    }
  }

  print('Reversed: ${sameNumberReversed(1256)}');
  print('NextPrimeReversed: ${nextPrimeAndReversed(1256)}');
/////////////////////////////////////////////////////////////////////////////////////
/* Fin Ejercicio 1*/ 

/* Ejercicio 2* */
/////////////////////////////////////////////////////////////////////////////////////
List<int> altura = [3,8,4,5,2]; // output 49

    int area(List<int> altura){
      int area = 0;
      int start = 0;
      int end = altura.length - 1;

      while(start < end){
        int largo = end - start;  // determina el ancho del arreglo

        // evaluar la altura de inicio con la final. si se cumple selecciona altura[start]
        int calculateAltura = altura[start] < altura[end]
          ? altura[start]
          : altura[end];

        int calculateWater = largo * calculateAltura;  //largo * alto

        // si se cumple tiene un area mayor, tien mas agua.
        if(calculateWater > area){    
          area = calculateWater;
        }

        // mover el indice del arreglo.
        if(altura[start] <= altura[end]){
          start++;
        } else {
          end--;
        }

      }

      return area;

    }

    int maxWater = area(altura);
  /////////////////////////////////////////////////////////////////////////////////
  /* Fin Ejercicio 2*/
  return MaterialApp(
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Coding Challenge'),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Center(
              child: Text('Se lee igual de izq a derecha y viceversa: ${sameNumberReversed(115511)}'),
            ),
            Center(
              child: Text('Siguiente numero primo: ${nextPrimeAndReversed(11)}'),
            ),
            Center(
              child: Text('Cantidad máxima de agua: $maxWater'),
            ),
          ],
        ),
      ),
    );
  }
}